const localtunnel = require('localtunnel')
const express = require("express")
var proxy = require('http-proxy-middleware');
const app = express()
const port = 3000;

app.get('/', proxy({
	target: 'http://192.168.1.166:8080',
	changeOrigin:true,
	autoRewrite: true
}))

app.listen(port, function (err) {
	console.log('server online')
})

const tunnel = openTunnel(port)

tunnel.on('close', function () {
	console.log('closed this tunnel')
})

tunnel.on('error', function (err) {
	console.log(err)

}) 

tunnel.on('request', function () {
	console.log('requested')
})

function openTunnel (port) {
	return localtunnel(port, { subdomain: "dirad" }, (err, tunnel) => {
		if (err) {
			return console.error(err)
		}
	
		console.log({dirad: tunnel.url})
	})
}
